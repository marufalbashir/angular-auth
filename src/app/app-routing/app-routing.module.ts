import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router';
import {HomeComponent} from '../home/home.component';
import {LoginComponent} from '../login/login.component';
import {DashboardComponent} from '../dashboard/dashboard.component';
import { NotFoundComponent } from '../not-found/not-found.component';
const routes:Routes=[
 { path: '', component:HomeComponent },
 { path: 'login', component:LoginComponent },
 { path: 'dashboard', component:DashboardComponent },
 { path: '**', component:NotFoundComponent },

];
// @NgModule({
//   declarations: [],
//   imports: [
//     CommonModule
//   ]
  
// })
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],

  exports:[RouterModule]

})export class AppRoutingModule { }
export const RoutingComponents=[HomeComponent,LoginComponent,DashboardComponent,NotFoundComponent]; 

