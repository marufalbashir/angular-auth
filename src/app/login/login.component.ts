import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  log(x){
    console.log(x);
  }
  submit(myForm){
    alert(myForm);
    console.log(myForm);
  }
  // new code
  constructor(
    private router: Router,
    private authService : AuthService,
  ) { }

  invalidLogin: boolean;
  signIn(credentials){
    this.authService.login(credentials).subscribe(result=>{
      if(result)
      this.router.navigate(['/']);
      else
      this.invalidLogin= true;
    })
  }
  ngOnInit(): void {
  }
}
