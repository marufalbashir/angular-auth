import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule  } from '@angular/forms';
import { AppComponent } from './app.component';
import { Http } from '@angular/common/http';
// import { LoginComponent } from './login/login.component';
// import { HomeComponent } from './home/home.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule, RoutingComponents } from './app-routing/app-routing.module';
//import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    // LoginComponent,
    // HomeComponent,
    // DashboardComponent
    RoutingComponents,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    Http,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
